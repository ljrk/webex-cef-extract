#! /bin/sh

PAK_UTIL=../submodules/chromium-grit/pak_util.py
SRC_PACKAGE=../package

find "$SRC_PACKAGE" -name '*.pak' -exec cp {} . \;

detect() {
    f="$1"
    ftype=$(xdg-mime query filetype $f)
    case "$ftype" in
    'application/gzip')
        if [ ! -f "$f.unzipped" ]; then
            gunzip -kc - < "$f" > "$f.unzipped"
        fi
        detect "$f.unzipped"
        ;;
    'text/plain')
        case "${PWD##*/}" in
        [a-z][a-z]-[A-Z][A-Z])
            # Matches e.g., en-US, en-GB, ...
            ln -sf "$f" "$f.txt"
            ;;
        *)
            # If not in a translation dir, assume JavaScript
            echo "Guessing $f with $ftype to be JS" >&2
            ln -sf "$f" "$f.js"
            ;;
        esac
        ;;
    'text/html')
        ln -sf "$f" "$f.html"
        ;;
    'text/x-Algol68'|'text/x-csrc'|'text/x-c++'|'text/x-java'|'application/x-perl')
        # These are actually a mix of JS (and some CSS, JSON)
        echo "Guessing $f with $ftype to be JS" >&2
        #ln -sf "$f" "$f.java"
        #ln -sf "$f" "$f.c"
        #ln -sf "$f" "$f.cc"
        #ln -sf "$f" "$f.a86"
        ln -sf "$f" "$f.js"
        ;;
    'text/x-asm')
        # This is actually CSS :-)
        echo "Guessing $f with $ftype to be JS" >&2
        #ln -sf "$f" "$f.asm"
        ln -sf "$f" "$f.css"
        ;;
    'application/json')
        ln -sf "$f" "$f.json"
        ;;
    'image/png')
        ln -sf "$f" "$f.png"
        ;;
    'image/avif')
        ln -sf "$f" "$f.avif"
        ;;
    'image/svg+xml')
        ln -sf "$f" "$f.svg"
        ;;
    'audio/flac')
        ln -sf "$f" "$f.flac"
        ;;
    'font/woff2')
        ln -sf "$f" "$f.woff"
        ;;
    'message/news'|'message/rfc822'|'application/mbox')
        # Translationfiles
        ln -sf "$f" "$f.txt"
        ;;
    'inode/x-empty')
        ln -sf "$f" "$f.inode"
        ;;
    'application/octet-stream')
        ln -sf "$f" "$f.bin"
        ;;
    *)
        echo "Unknown file-type $ftype of file $f" >&2
        ;;
    esac
}

for pakfile in $(find . -maxdepth 1 -name '*.pak'); do
    mkdir -p "${pakfile%.pak}";
    (
        cd "${pakfile%.pak}"
        python2 "../$PAK_UTIL" extract "../$pakfile"
        for x in $(find . -type f -not -name '*.*'); do
            detect "$x"
        done
    )
done
