# Cisco WebEx (Linux) CEF Application extractor

## Usage

1. Clone the repo with `--recurse-submodules`
2. Extract a built
   [webex-bin](https://aur.archlinux.org/packages/webex-bin/)
   package
   (which repackages the
   [official Debian package](https://binaries.webex.com/WebexDesktop-Ubuntu-Official-Package/Webex.deb))
   into a directory called "package" next to the "PAK" directory.
3. You can now enter the PAK directory and call the `./extract.sh` file.

